# Mac Citrix Receiver Uninstaller

## Installation

1. Download the zip file from here: [https://bitbucket.org/benniemosher/citrix-receiver-uninstaller/downloads]
2. Extract the zip file
3. Quit out of Citrix Receiver and any related applications
4. In terminal, navigate inside of your extracted folder
5. Run `chmod +x uninstaller.sh`
6. Run `sh uninstaller.sh`
	* This will ask for your password, to remove the files not in your home directory, you are required to run as root so it asks for your password.
7. Go to `~/Applications` and remove any applications Citrix Receiver installed
8. You should now have fully uninstalled the Citrix Receiver app for Mac

## This installer removes these files

```
/Applications/Citrix\ Receiver.app
/Library/Internet plug-ins/CitrixICAClientPlugIn.plugin
/Library/LaunchAgents/com.citrix.AuthManager_Mac.plist
/Library/LaunchAgents/com.citrix.ReceiverHelper.plist
/Library/LaunchAgents/com.citrix.ServiceRecords.plist
/Library/PreferencePanes/FMDSysPrefPane.prefPane
/private/var/db/receipts/com.citrix.ICAClient.bom
/private/var/db/receipts/com.citrix.ICAClient.plist
/private/var/db/receipts/com.citrix.ShareFile.installer.bom
/private/var/db/receipts/com.citrix.ShareFile.installer.plist
/Users/Shared/Citrix/Receiver\ Integration/

~/Applications/Citrix/FollowMeData
~/Library/Application Support/Citrix Receiver/
~/Library/Application Support/Citrix/
~/Library/Application Support/ShareFile
~/Library/Internet plug-ins/CitrixICAClientPlugIn.plugin
~/Library/Preferences/com.citrix.AuthManagerMac.plist
~/Library/Preferences/com.citrix.receiver.nomas.plist
~/Library/Preferences/com.citrix.receiver.nomas.plist.lockfile
~/Library/Preferences/com.citrix.ReceiverFTU.AccountRecords.plist.lockfile
```