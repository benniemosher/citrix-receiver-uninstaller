# SEE: http://support.citrix.com/article/CTX134237

sudo rm -rf /Applications/Citrix\ Receiver.app

sudo rm -rf /Library/Internet plug-ins/CitrixICAClientPlugIn.plugin
sudo rm -rf /Library/LaunchAgents/com.citrix.AuthManager_Mac.plist
sudo rm -rf /Library/LaunchAgents/com.citrix.ReceiverHelper.plist
sudo rm -rf /Library/LaunchAgents/com.citrix.ServiceRecords.plist

sudo rm -rf /Users/Shared/Citrix/Receiver\ Integration/

rm -rf ~/Library/Internet plug-ins/CitrixICAClientPlugIn.plugin
rm -rf ~/Library/Application Support/Citrix/
rm -rf ~/Library/Application Support/Citrix Receiver/
rm -rf ~/Library/Preferences/com.citrix.AuthManagerMac.plist
rm -rf ~/Library/Preferences/com.citrix.receiver.nomas.plist
rm -rf ~/Library/Preferences/com.citrix.receiver.nomas.plist.lockfile
rm -rf ~/Library/Preferences/com.citrix.ReceiverFTU.AccountRecords.plist.lockfile

sudo rm -rf /private/var/db/receipts/com.citrix.ICAClient.bom
sudo rm -rf /private/var/db/receipts/com.citrix.ICAClient.plist

# Follow Me Data
rm -rf ~/Applications/Citrix/FollowMeData
rm -rf ~/Library/Application Support/ShareFile
sudo rm -rf /Library/PreferencePanes/FMDSysPrefPane.prefPane

sudo rm -rf /private/var/db/receipts/com.citrix.ShareFile.installer.plist
sudo rm -rf /private/var/db/receipts/com.citrix.ShareFile.installer.bom

echo "\n\nPlease go into ~/Applications/ and remove any applications that Citrix Receiver installed."
